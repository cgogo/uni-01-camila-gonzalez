<%-- 
    Document   : resultado
    Created on : 11-04-2021, 13:24:55
    Author     : cGoGo
--%>

<%@page import="modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora calc = (Calculadora)request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>El interés simple es:</h1>
        <h2><%= calc.getResultado() %></h2>
    </body>
</html>
