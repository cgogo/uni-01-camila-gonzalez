<%-- 
    Document   : index
    Created on : 11-04-2021, 13:24:41
    Author     : cGoGo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Calcula el interés simple</h1>
        <form  name="form" action="CalculadoraController" method="POST">
            <p>Ingresa el valor del capital</p>
            <input type="number" id="capital" name="capital">
            <p>Ingresa el porcentaje de la tasa de interés</p>
            <input type="number" id="tasa" name="tasa">
            <p>Ingresa el número de años</p>
            <input type="number" id="anio" name="anio">
            <button type="submit" name="calcular">Calcular</button>
        </form>
    </body>
</html>